# Approx-K-Centers

K centros aproximados - TP2 de Algoritmos 2

## Fluxo de Construção do Trabalho e Organização dos Diretórios
- Primeiro os dados brutos foram tratados no diretório "Limpeza dos Dados", cada dataset foi manualmente processado conforme especificado no Relatório. Nesse diretório em cada pasta está o dataset original, o dataset resultante e o notebook utilizado para processar o dataset orignal e, consequentemente, construir o dataset processado
    - Os datasets processados começam com a substring "clean_" e possuem extensão .csv
- Em seguida ocorreu a Execução dos experimentos. Embora exista um diretório "Execução dos Experimento", essa etapa foi realizada por meio do Google Colab. Portanto, nessa pasta estão todos os arquivos necessários para executar o notebook proveniente do Google Colab.
    - Observe que ao executar o notebook dessa pasta em um dos datasets limpos será gerado um arquivo report.csv. Esses arquivos serão utilizados na etapa Agregação dos Resultados.
- Logo após, os resultados foram agregados e analisados no diretório "Agregação dos Resultados".
    - Observe que o nome dos arquivos é 'reportk.csv', onde k é o id do dataset. O id foi feito para automatizar o processo de concatenação dos resultados, mas foi construído basicamente atribuindo números de 0 a 9 para os datasets na ordem lexicográfica dos diretórios dentro do diretório 'Limpeza dos Dados'
- O diretório "Imagens do Relatório" contém as imagens que foram utilizadas na construção do relatório.